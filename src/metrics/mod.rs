use std::sync::atomic::*;

pub static CLIENT_OBJ_COUNT             : AtomicUsize = AtomicUsize::new(0);
pub static WS_CONNECTION                : AtomicUsize = AtomicUsize::new(0);
pub static TCP_CONNECTION               : AtomicUsize = AtomicUsize::new(0);
pub static PLUGIN_REQUESTS              : AtomicUsize = AtomicUsize::new(0);

pub static PUBLISH_COUNT                : AtomicUsize = AtomicUsize::new(0);
pub static CLIENT_WAKE_COUNT            : AtomicUsize = AtomicUsize::new(0);

pub static WRITE_COUNT                  : AtomicUsize = AtomicUsize::new(0);
pub static WRITE_COUNT_BIG              : AtomicUsize = AtomicUsize::new(0);


#[macro_export]
macro_rules! increase_counter {
    ($ctr:path) => {
        $ctr.fetch_add(1, std::sync::atomic::Ordering::Relaxed)
    };
}

#[macro_export]
macro_rules! increase_counter_by {
    ($ctr:path, $val:expr) => {
        $ctr.fetch_add($val, std::sync::atomic::Ordering::Relaxed)
    };
}

#[macro_export]
macro_rules! decrease_counter {
    ($ctr:path) => {
        $ctr.fetch_sub(1, std::sync::atomic::Ordering::Relaxed)
    }
}

#[macro_export]
macro_rules! get_counter {
    ($ctr:path) => {
        $ctr.load(std::sync::atomic::Ordering::Relaxed)
    }
}

pub struct BrokerMetrics {
    pub ws_connection_so_far    : usize,
    pub tcp_connection_so_far   : usize,
    pub publish_cnt             : usize,
    pub client_wake_cnt         : usize,
    pub write_cnt               : usize,
    pub write_big_cnt           : usize,
}

impl BrokerMetrics {
    pub fn current() -> Self {
        Self {
            ws_connection_so_far    : get_counter!(WS_CONNECTION),
            tcp_connection_so_far   : get_counter!(TCP_CONNECTION),
            publish_cnt             : get_counter!(PUBLISH_COUNT),
            client_wake_cnt         : get_counter!(CLIENT_WAKE_COUNT),
            write_cnt               : get_counter!(WRITE_COUNT),
            write_big_cnt           : get_counter!(WRITE_COUNT_BIG),
        }
    }
}
