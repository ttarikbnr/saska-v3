use std::io;
use std::fmt::{self, Display, Formatter};
use mqtt::packet::{PacketError, VariablePacketError, Packet};
use mqtt::control::fixed_header::FixedHeaderError;
use mqtt::control::variable_header::VariableHeaderError;
use mqtt::encodable::StringEncodeError;
// use mqtt::topic_name::TopicNameError;

#[derive(Debug)]
pub enum SocketError {
    IoError,
    FixedHeaderError(FixedHeaderError),
    VariableHeaderError,
    PayloadError,
    MalformedPacketError,
    StringEncodeError,
    TopicNameError,
    UnrecognizedPacketError,
    ReservedPacketError
}

impl From<io::Error> for SocketError {
    fn from(_error: io::Error) -> Self {
        SocketError::IoError
    }
}

impl From<VariablePacketError> for SocketError {
    
    fn from(error: VariablePacketError) -> Self {
        match error {
            VariablePacketError::IoError(_io_error) => {
                Self::IoError
            }
            VariablePacketError::FixedHeaderError(fixed_header_error) => {
                Self::FixedHeaderError(fixed_header_error)
            }
            VariablePacketError::UnrecognizedPacket(_, _) => {
                Self::UnrecognizedPacketError
            },
            VariablePacketError::ReservedPacket(_, _) => {
                Self::ReservedPacketError
            }
            VariablePacketError::ConnectPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::ConnackPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::PublishPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::PubackPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::PubrecPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::PubrelPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::PubcompPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::PingreqPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::PingrespPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::SubscribePacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::SubackPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::UnsubscribePacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::UnsubackPacketError(packet_error) => {
                packet_error.into()
            },
            VariablePacketError::DisconnectPacketError(packet_error) => {
                packet_error.into()
            },
        }
    }
}


impl <T> From<PacketError<T>> for SocketError 
    where T: Packet + 'static{
    fn from(error: PacketError<T>) -> Self {
        match error {
            PacketError::IoError(_io_error) => { 
                Self::IoError
            },
            PacketError::FixedHeaderError(fixed_header_error) => {
                match &fixed_header_error {
                    FixedHeaderError::IoError(_io_error) => {
                        Self::IoError
                    }
                    _rest => {
                        Self::FixedHeaderError(fixed_header_error)
                    }
                }
            },
            PacketError::VariableHeaderError(variable_header_error) => {
                match variable_header_error {
                    VariableHeaderError::IoError(_io_error) => {
                        Self::IoError
                    }
                    _rest => {
                        Self::VariableHeaderError
                    }
                }
            },
            PacketError::PayloadError(_payload_error) => {
                Self::PayloadError
            },
            PacketError::MalformedPacket(_malformed_packet_error) => {
                Self::MalformedPacketError
            },
            PacketError::StringEncodeError(string_encode_error) => {
                match string_encode_error {
                    StringEncodeError::IoError(_io_error) => {
                        Self::IoError
                    }
                    _rest => {
                        Self::StringEncodeError
                    }
                }
            },
            PacketError::TopicNameError(_topic_name_error) => {
                Self::TopicNameError
            },
        }
    }
}


impl Display for SocketError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let variant_str = match self {
            SocketError::IoError => {
                "IoError".to_owned()
            },
            SocketError::FixedHeaderError(fixed_header_error) => {
                format!("FixedHeaderError: {}", fixed_header_error)
            },
            SocketError::VariableHeaderError => {
                "VariableHeaderError".to_owned()
            },
            SocketError::PayloadError => {
                "PayloadError".to_owned()
            },
            SocketError::MalformedPacketError => {
                "MalformedPacketError".to_owned()
            },
            SocketError::StringEncodeError => {
                "StringEncodeError".to_owned()
            },
            SocketError::TopicNameError => {
                "TopicNameError".to_owned()
            },
            SocketError::UnrecognizedPacketError => {
                "UnrecognizedPacketError".to_owned()
            },
            SocketError::ReservedPacketError => {
                "ReservedPacketError".to_owned()
            }
        };

        write!(f, "{}", variant_str)
    }
}