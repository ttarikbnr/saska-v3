use tokio::net;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use super::Socket;
use tokio_tungstenite::accept_hdr_async;
use tungstenite::handshake::server::{Callback, Request, Response, ErrorResponse};
use tungstenite::http::HeaderValue;
use crate::metrics;
use crate::conf;

pub struct TcpListener {
    listener: net::TcpListener,
}

impl TcpListener {
    pub async fn new(port: u16) -> Self {
        let socket_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), port);
        let tcp_socket = net::TcpSocket::new_v4()
            .expect("Can't build tcp listener socket.");
        if let Err(err) = tcp_socket.set_send_buffer_size(conf::get_socket_buffer_capacity()) {
            log::error!("Can't set send buffer size. {}", err)
        }
            
        tcp_socket.bind(socket_addr)
                    .expect("Can't bind tcp listener!");

        let listener = tcp_socket.listen(conf::get_backlog_size())
                    .expect("Can't get listener.");


        log::info!("Started listening tcp on port {}.", port);

        Self {
            listener,
        }
    }

    pub async fn accept(&self) -> Socket {
        loop {
            match self.listener.accept().await {
                Ok((socket, _addr)) => {
                    // log::info!("New incoming socket. Address of peer is {}", addr);
                    crate::increase_counter!(metrics::TCP_CONNECTION);
                    return Socket::new_tcp(socket)
                }
                Err(err) => {
                    log::error!("TcpListener: Couldn't get client: {:?}", err);
                }
            }
        }
    }
}

pub struct WebSocketListener {
    listener    : net::TcpListener,
}

impl WebSocketListener {
    pub async fn new(port: u16) -> Self {
        let socket_addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), port);

        let tcp_socket = net::TcpSocket::new_v4()
            .expect("Can't build tcp listener socket.");

        if let Err(err) = tcp_socket.set_send_buffer_size(conf::get_socket_buffer_capacity()) {
            log::error!("Can't set send buffer size. {}", err)
        }

        tcp_socket.bind(socket_addr)
                    .expect("Can't bind tcp listener!");

        let listener = tcp_socket.listen(conf::get_backlog_size())
                    .expect("Can't get listener.");

        log::info!("Started listening ws on port {}.", port);

        Self {
            listener,
        } 
    }

    pub async fn accept(&self) -> Socket {
        loop {
            match self.listener.accept().await {
                Ok((socket, addr)) => {
                    log::info!("New incoming socket. Address of peer is {}", addr);

                    match accept_hdr_async(socket, WsSubProtocolHandler).await {
                        Ok(websocket) => {
                            crate::increase_counter!(metrics::WS_CONNECTION);
                            return Socket::new_websocket(websocket)
                        }
                        Err(err) => {
                            log::error!("Got error while accepting websocket. {}", err); 
                        }
                    }
                }
                Err(err) => {
                    eprintln!("TcpListener: Couldn't get client: {:?}", err);
                    log::error!("TcpListener: Couldn't get client: {:?}", err);
                }
            }
        }
    }
}

pub struct Listener {
    tcp_listener        : TcpListener,
    websocket_listener  : WebSocketListener,
}

impl Listener {
    pub async fn bind(tcp_port: u16, websocket_port: u16) -> Self {
        let tcp_listener = TcpListener::new(tcp_port).await;
        let websocket_listener = WebSocketListener::new(websocket_port).await;
        Self {
            tcp_listener,
            websocket_listener,
        }
    }

    pub async fn accept(&self) -> Socket {
        tokio::select! {
            socket = self.tcp_listener.accept() => { return socket}
            socket = self.websocket_listener.accept() => { return socket}
        } 
    }
}


struct WsSubProtocolHandler;

impl Callback for WsSubProtocolHandler {
    fn on_request( self, _request: &Request, mut response: Response) -> Result<Response, ErrorResponse> {
        response.headers_mut().insert("Sec-WebSocket-Protocol", HeaderValue::from_static("mqttv3.1"));
        Ok(response)
    }
}