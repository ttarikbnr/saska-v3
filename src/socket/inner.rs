use super::error::SocketError;
use futures::{
    stream::{SplitSink, SplitStream, Stream},
    SinkExt, StreamExt,
};
use mqtt::packet::{VariablePacket, VariablePacketError};
use mqtt::Decodable;
use std::{error::Error, io::{
    self,
    Cursor
}};
use std::collections::VecDeque;
use tokio::io::{AsyncRead, AsyncWriteExt};
use tokio::net::TcpStream;
use tokio_tungstenite::{
    tungstenite::error::Error as WsError, tungstenite::Message, WebSocketStream,
};
use crate::conf;


pub enum SocketInner {
    Tcp(TcpStream),
    WebSocket(Box<WebSocketStream<TcpStream>>),
}

pub struct Socket {
    inner: SocketInner,
    read_buf: Vec<u8>,
    packet_buf: VecDeque<VariablePacket>
}

impl Socket {
    pub fn new_tcp(tcp_stream: TcpStream) -> Self {
        let inner = SocketInner::Tcp(tcp_stream);
        let read_buf = Vec::new();
        let packet_buf = VecDeque::new();
        Socket {
            inner,
            read_buf,
            packet_buf
        }
    }

    pub fn new_websocket(websocket: WebSocketStream<TcpStream>) -> Self {
        let inner = SocketInner::WebSocket(Box::new(websocket));
        let read_buf = Vec::new();
        let packet_buf = VecDeque::new();
        Socket {
            inner,
            read_buf,
            packet_buf
        }
    }

    pub async fn write_all(&mut self, src: &[u8]) -> Result<(), SocketError>
    where
        Self: Unpin,
    {
        match self.inner {
            SocketInner::Tcp(ref mut tcp_stream) => tcp_stream.write_all(src).await.map_err(|err| err.into()),
            SocketInner::WebSocket(ref mut websocket) => {
                let message = Message::Binary(src.to_vec());
                if let Err(err) = websocket.send(message).await {
                    if let WsError::Io(err) = err {
                        return Err(err.into());
                    } else {
                        let err = format!("Websocket error {}.", err);
                        return Err(io::Error::new(io::ErrorKind::Other, err).into());
                    }
                }
                Ok(())
            }
        }
    }

    pub async fn read(&mut self) -> Result<VariablePacket, SocketError>
    where
        Self: Unpin,
    {
        match self.inner {
            SocketInner::Tcp(ref mut tcpstream) => read_from_tcpsocket(tcpstream).await,
            SocketInner::WebSocket(ref mut websocket) => read_from_websocket(websocket,
                                                            &mut self.read_buf,
                                                            &mut self.packet_buf).await,
        }
    }

    pub fn split(&mut self) -> (SocketReadHalf, SocketWriteHalf) {
        match &mut self.inner {
            SocketInner::Tcp(tcpstream) => {
                let (rh, wh) = tcpstream.split();

                (
                    SocketReadHalf::new(SocketReadHalfInner::Tcp(rh),
                                        self.read_buf.drain(..).collect(),
                                        self.packet_buf.drain(..).collect()),
                    SocketWriteHalf::new(SocketWriteHalfInner::Tcp(wh)),
                )
            }
            SocketInner::WebSocket(websocket) => {
                use std::borrow::BorrowMut;
                let borrowed : &mut tokio_tungstenite::WebSocketStream<tokio::net::TcpStream> = websocket.borrow_mut();
                let (sink, stream) = borrowed.split();
                (
                    SocketReadHalf::new(SocketReadHalfInner::WebSocket(stream),
                                        self.read_buf.drain(..).collect(),
                                        self.packet_buf.drain(..).collect()),
                    SocketWriteHalf::new(SocketWriteHalfInner::WebSocket(sink)),
                )
            }
        }
    }

    #[allow(unused)]
    pub async fn shutdown(&mut self) -> Result<(), SocketError>
    where
        Self: Unpin,
    {
        match self.inner {
            SocketInner::Tcp(ref mut tcp_stream) => tcp_stream.shutdown().await.map_err(|err| err.into()),
            SocketInner::WebSocket(ref mut websocket) => {
                if let Err(err) = websocket.close().await {
                    if let WsError::Io(err) = err {
                        return Err(err.into());
                    } else {
                        let err = format!("Websocket error {}.", err);
                        return Err(io::Error::new(io::ErrorKind::Other, err).into());
                    }
                }
                Ok(())
            }
        }
    }
}

pub enum SocketReadHalfInner<'a> {
    Tcp(tokio::net::tcp::ReadHalf<'a>),
    WebSocket(SplitStream<&'a mut WebSocketStream<TcpStream>>),
}

pub struct SocketReadHalf<'a> {
    inner: SocketReadHalfInner<'a>,
    read_buf: Vec<u8>,
    packet_buf: VecDeque<VariablePacket>,
}

impl <'a>SocketReadHalf<'a> {
    pub fn new(inner        : SocketReadHalfInner<'a>,
               read_buf     : Vec<u8>,
               packet_buf   : VecDeque<VariablePacket>) -> Self {
        Self { 
            inner,
            read_buf,
            packet_buf,
        }
    }

    pub async fn read(&mut self) -> Result<VariablePacket, SocketError>
    where
        Self: Unpin,
    {
        match self.inner {
            SocketReadHalfInner::Tcp(ref mut tcpstream) => read_from_tcpsocket(tcpstream)
                .await,
            SocketReadHalfInner::WebSocket(ref mut websocket) => {
                read_from_websocket(websocket, &mut self.read_buf, &mut self.packet_buf)
                    .await
            }
        }
    }
}

pub enum SocketWriteHalfInner<'a> {
    Tcp(tokio::net::tcp::WriteHalf<'a>),
    WebSocket(SplitSink<&'a mut WebSocketStream<TcpStream>, Message>),
}

pub struct SocketWriteHalf<'a> {
    inner: SocketWriteHalfInner<'a>,
}

impl <'a>SocketWriteHalf<'a> {
    pub fn new(inner: SocketWriteHalfInner<'a>) -> Self {
        Self { inner }
    }

    pub async fn write_all(&mut self, src: &[u8]) -> Result<(), SocketError>
    where
        Self: Unpin,
    {
        match self.inner {
            SocketWriteHalfInner::Tcp(ref mut tcp_stream) => tcp_stream.write_all(src).await.map_err(SocketError::from),
            SocketWriteHalfInner::WebSocket(ref mut websocket) => {
                let message = Message::Binary(src.to_vec());
                if let Err(err) = websocket.send(message).await {
                    if let WsError::Io(err) = err {
                        Err(err.into())
                    } else {
                        let err = format!("Websocket error {}.", err);
                        Err(io::Error::new(io::ErrorKind::Other, err).into())
                    }
                } else {
                    Ok(())

                }
            }
        }
    }

    pub async fn shutdown(&mut self) {
        match self.inner {
            SocketWriteHalfInner::Tcp(ref mut tcp_stream) => {
                if let Err(err) = tcp_stream.shutdown().await {
                    log::info!("Got error while shutting down the tcp socket.\n{}", err)
                }
            }
            SocketWriteHalfInner::WebSocket(ref mut websocket) => {
                if let Err(err) = websocket.close().await {
                    log::info!(
                        "Got error while shutting down the websocket socket.\n{}",
                        err
                    )
                }
            }
        }
    }
}

async fn read_from_tcpsocket<T>(tcpstream: &mut T) -> Result<VariablePacket, SocketError>
where
    T: AsyncRead + Unpin,
{
    match VariablePacket::parse(tcpstream).await {
        Ok(packet) => return Ok(packet),
        Err(err) => {
            Err(err.into())
        }
    }
}

async fn read_from_websocket<T>(websocket: &mut T,
                                read_buf: &mut Vec<u8>,
                                packet_buf: &mut VecDeque<VariablePacket>) -> Result<VariablePacket, SocketError>
where
    T: Stream<Item = Result<Message, WsError>> + Unpin,
{
    'outer: loop {
        if let Some(packet) = packet_buf.pop_back() {
            return Ok(packet)
        }

        match websocket.next().await {
            Some(Ok(Message::Binary(packet_raw))) => {
                read_buf.extend(packet_raw.as_slice());

                loop {
                    let mut read_buf_cursor = Cursor::new(&read_buf);
                    match VariablePacket::decode(&mut read_buf_cursor) {
                        Ok(packet) => {
                            let pos = read_buf_cursor.position();
                            drop(read_buf_cursor);
                            *read_buf = read_buf.split_off(pos as _);
                            packet_buf.push_front(packet);
                            continue
                        },
                        Err(err) => {

                            if is_unexpected_eof(&err) {
                                continue 'outer
                            } 

                            if conf::get_continue_when_fixed_header_error() &&
                                    is_fixed_header_error(&err) {
                                continue 'outer
                            }
                            let err_msg = format!("Mqtt packet decoding error. {:?}", err);
                            
                            return Err(io::Error::new(io::ErrorKind::Other, err_msg).into())
                        }
                    }
                }
            }
            Some(Ok(_msg)) => {
                let err = "Unsupported websocket message".to_string();
                return Err(io::Error::new(io::ErrorKind::Other, err).into())
            }
            Some(Err(err)) => {
                let err = format!("Got error from websocket {}", err);
                return Err(io::Error::new(io::ErrorKind::Other, err).into())
            }
            None => {
                let err = "Websocket connection is closed".to_string();
                return Err(io::Error::new(io::ErrorKind::Other, err).into())
            }
        }
    }
}

fn is_unexpected_eof(err: &VariablePacketError) -> bool {
    let mut source = err.source();
    while let Some(inner_source) = source {
        if inner_source.source().is_none() {
            break
        }
        source = inner_source.source();
    }
    if let Some(source_error) = source {
        if let Some(io_error) = source_error.downcast_ref::<std::io::Error>(){
            if let std::io::ErrorKind::UnexpectedEof = io_error.kind() {
                return true
            }
        }
    }
    false
}


fn is_fixed_header_error(err: &VariablePacketError) -> bool {
    if let VariablePacketError::FixedHeaderError(_) = err {
        return true
    }
    false
}
