use config::{Config as Cfg, File, FileFormat};

static mut CONFIG : Config = Config::default();


struct Config {
    tcp_listening_port          : u16,
    ws_listening_port           : u16,
    data_flush_period_ms        : u64,
    client_min_wake_dur_ms      : u64,
    command_channel_cap         : usize,
    connect_fut_timeout_sec     : u64,
    write_buffered              : bool,
    write_buffer_cap            : usize,
    socket_buffer_cap           : u32,
    force_disconnect_delay_sec  : u64,
    backlog_size                : u32,
    continue_when_fixed_header_error : bool,
}

impl Config {
    const fn default() -> Self {
        Self {
            tcp_listening_port          : 3445,
            ws_listening_port           : 8114,
            data_flush_period_ms        : 100,
            client_min_wake_dur_ms      : 70,
            command_channel_cap         : 1024,
            connect_fut_timeout_sec     : 10,
            write_buffered              : true,
            write_buffer_cap            : 32 * 1024,
            socket_buffer_cap           : 128 * 1024,
            force_disconnect_delay_sec  : 4,
            backlog_size                : 1024,
            continue_when_fixed_header_error : false,
        }
    }

    fn from_cfg(cfg: Cfg) -> Self {
        Self {
            tcp_listening_port: cfg.get::<u16>("tcp_listening_port")
                .expect("Can't find tcp listening port in config."),
            ws_listening_port : cfg.get::<u16>("websocket_listening_port")
                .expect("Can't find ws listening port in config."),
            data_flush_period_ms : cfg.get::<u64>("data_flush_period_ms")
                .expect("Can't find data flush period in config."),
            client_min_wake_dur_ms : cfg.get::<u64>("client_min_wake_dur_ms")
                .expect("Can't find client min wake period in config."),
            command_channel_cap : cfg.get::<usize>("command_channel_capacity")
                .expect("Can't find command channel capacity in config."),
            connect_fut_timeout_sec : cfg.get::<u64>("connect_fut_timeout_sec")
                .expect("Can't find connection timeout duration in config."),
            write_buffered   : cfg.get::<bool>("write_buffered")
                .expect("Can't find write buffered in config."),         
            write_buffer_cap : cfg.get::<usize>("write_buffer_capacity")
                .expect("Cant' find write buffer capacity in config."),
            socket_buffer_cap: cfg.get::<u32>("socket_buffer_capacity")
                .expect("Can't find socket buffer capacity in config."),
            force_disconnect_delay_sec: cfg.get::<u64>("force_disconnect_delay_sec")
                .expect("Can't find force disconnect delay sec in config."),
            backlog_size     : cfg.get::<u32>("backlog_size")
                .expect("Can't find backlog size in config."),
            continue_when_fixed_header_error : cfg.get::<bool>("continue_when_fixed_header_error")
                .unwrap_or(false)
        }
    }
}


pub fn create_config<P: AsRef<str>>(config_path: P) {
    let mut cfg = Cfg::default();
    cfg.merge(File::new(config_path.as_ref(), FileFormat::Toml))
        .expect("Couldn't create config!");

    let config = Config::from_cfg(cfg);
    unsafe {
        CONFIG = config;
    }
}

#[inline(always)]
pub fn get_tcp_listening_port() -> u16 {
    unsafe {
        CONFIG.tcp_listening_port
    }
}

#[inline(always)]
pub fn get_ws_listening_port() -> u16 {
    unsafe {
        CONFIG.ws_listening_port
    }
}

#[inline(always)]
pub fn get_data_flush_period_ms() -> u64{
    unsafe {
        CONFIG.data_flush_period_ms
    }
}

#[inline(always)]
pub fn get_client_min_wake_dur_ms() -> u64 {
    unsafe {
        CONFIG.client_min_wake_dur_ms
    }
}

#[inline(always)]
pub fn get_connect_fut_timeout() -> u64 {
    unsafe {
        CONFIG.connect_fut_timeout_sec
    }
}

#[inline(always)]
pub fn get_command_channel_capacity() -> usize {
    unsafe {
        CONFIG.command_channel_cap
    }
}

#[inline(always)]
pub fn get_write_buffered() -> bool {
    unsafe {
        CONFIG.write_buffered
    }
}

#[inline(always)]
pub fn get_write_buffer_capacity() -> usize {
    unsafe {
        CONFIG.write_buffer_cap
    }
}

#[inline(always)]
pub fn get_socket_buffer_capacity() -> u32 {
    unsafe {
        CONFIG.socket_buffer_cap
    }
}

#[inline(always)]
pub fn get_force_disconnect_delay_sec() -> u64 {
    unsafe {
        CONFIG.force_disconnect_delay_sec
    }
}

#[inline(always)]
pub fn get_backlog_size() -> u32 {
    unsafe {
        CONFIG.backlog_size
    }
}

#[inline(always)]
pub fn get_continue_when_fixed_header_error() -> bool {
    unsafe {
        CONFIG.continue_when_fixed_header_error
    }
}