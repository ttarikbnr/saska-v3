use std::sync::{Arc, atomic::{AtomicUsize, Ordering}};
use std::{pin::Pin, task::{Poll, Context}};
use futures::task::AtomicWaker;
use futures::stream::Stream;
use crossbeam_utils::CachePadded;
use crate::conf;
// use crate::metrics;

const CAP               : usize = 1024;
const MASK              : usize = CAP - 1;

/// Size must be always power of 2
struct CircularBuffer<T> {
    read_idx    : CachePadded<AtomicUsize>,
    buffer      : *mut T,
    write_idx   : CachePadded<AtomicUsize>, 
}

unsafe impl <T>Send for CircularBuffer<T> {}
unsafe impl <T>Sync for CircularBuffer<T> {}


impl <T> CircularBuffer<T>{
    #[inline(always)]
    fn new() -> Self {
        let mut v = Vec::with_capacity(CAP as _);
        let buffer = v.as_mut_ptr();
        std::mem::forget(v);

        Self {
            buffer,
            read_idx    : CachePadded::new(AtomicUsize::new(0)),
            write_idx   : CachePadded::new(AtomicUsize::new(0)),
        }
    }

    #[inline(always)]
    #[allow(clippy::ptr_offset_with_cast)]
    fn push(&self, item: T) -> bool {
        let write_idx = self.write_idx.load(Ordering::Acquire);
    
        let write_idx_next = (write_idx + 1) & MASK;
        if write_idx_next != self.read_idx.load(Ordering::Acquire) {
            unsafe {
                self.buffer.offset(write_idx as isize)
                    .write_volatile(item);
            }
    
            self.write_idx.store(write_idx_next, Ordering::Release);
            return true
        }

        false
    }

    #[inline(always)]
    #[allow(clippy::ptr_offset_with_cast)]
    fn pop(&self) -> Option<T> {
        let read_idx    = self.read_idx.load(Ordering::Acquire);
        let write_idx   = self.write_idx.load(Ordering::Acquire);
        if read_idx == write_idx {
            return None;
        }

        let next_read_idx = (read_idx + 1) & MASK;

        self.read_idx.store(next_read_idx, Ordering::Release);

        unsafe {
            Some(self.buffer.offset(read_idx as isize)
                           .read_volatile())
        }
    }
}

impl <T>Drop for CircularBuffer<T> {
    fn drop(&mut self) {
        unsafe {

            // Consume items
            // Doing this we call each items destructor
            while self.pop().is_some() {}

            Vec::from_raw_parts(self.buffer, 0, CAP as _);
        }
    }
}
struct Inner<T> {
    circ_buff   : CircularBuffer<T>,
    recv_waker  : AtomicWaker,
    is_closed   : std::cell::UnsafeCell<bool>,
}

unsafe impl <T>Send for Inner<T> {}
unsafe impl <T>Sync for Inner<T> {}

impl <T> Inner <T> {

    #[inline(always)]
    fn set_closed(&self) {
        unsafe {
            *self.is_closed.get() = false;
        }
    }

    #[inline(always)]
    fn is_closed(&self) -> bool {
        unsafe {
            *self.is_closed.get()
        }
    }
}

pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let inner = Inner {
        circ_buff   : CircularBuffer::new(),
        recv_waker  : AtomicWaker::new(),
        is_closed   : std::cell::UnsafeCell::new(false)
    };
    let inner_arc = Arc::new(inner);
    let sender = Sender::new(inner_arc.clone());
    let receiver = Receiver::new(inner_arc);

    (sender, receiver)
}

pub struct Receiver<T> {
    inner: Arc<Inner<T>>,
}

unsafe impl <T> Send for Receiver<T> {}

impl <T>Receiver<T> {
    fn new(inner: Arc<Inner<T>>) -> Self {
        Self {
            inner,
        }
    }

    #[inline(always)]
    pub fn recv(&self) -> Option<T> {
        self.inner.circ_buff.pop()
    }
}

impl <T> Drop for Receiver<T> {
    fn drop(&mut self) {
        self.inner.set_closed();
    }
}

impl <T> Stream for Receiver<T> {
    type Item = T;
    
    #[inline(always)]
    fn poll_next(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {

        let pop_res = self.inner.circ_buff.pop();
        if pop_res.is_some() {
            return Poll::Ready(pop_res)
        }

        self.inner.recv_waker.register(cx.waker());

        let pop_res = self.inner.circ_buff.pop();
        if pop_res.is_some() {
            Poll::Ready(pop_res)
        } else {
            Poll::Pending
        }
    }
}

use std::cell::Cell;

pub struct Sender <T> {
    inner        : Arc<Inner<T>>,
    should_flush : Cell<bool>,
    last_waked   : Cell<u64>
}

impl <T> Sender <T> {
    
    #[inline(always)]
    fn should_wake(&self, now: u64) -> bool {

        let last_waked = self.last_waked.get();

        now - last_waked > conf::get_client_min_wake_dur_ms()
    }

    #[inline(always)]
    fn set_last_waked(&self, now: u64) {
        self.last_waked.set(now);
    }

    #[inline(always)]
    pub fn notify(&self, now: u64) {
        if self.should_wake(now) {
            // crate::increase_counter!(metrics::CLIENT_WAKE_COUNT);
            self.inner.recv_waker.wake();
            self.set_last_waked(now);
            self.should_flush.set(false);
        }
    }

    #[inline(always)]
    pub fn flush(&self, now: u64)  {
        if self.should_flush.get() {
            // crate::increase_counter!(metrics::CLIENT_WAKE_COUNT);
            self.inner.recv_waker.wake();
            self.set_last_waked(now);
            self.should_flush.set(false);
        }
    }
}

// unsafe impl <T> Send for Sender<T> {}

impl <T>Sender<T> {
    fn new(inner: Arc<Inner<T>>) -> Self {
        Self {
            inner,
            should_flush: Cell::new(false),
            last_waked  : Cell::new(0),
        }
    }

    #[inline(always)]
    pub fn send(&self, item: (u64, T)) -> Result<(), SenderError> {
        if self.inner.is_closed() {
            return Err(SenderError::Closed)
        }
        
        let ret = if self.inner.circ_buff.push(item.1) {
            Err(SenderError::Full)
        } else {
            Ok(())
        };
        
        self.should_flush.set(true);

        self.notify(item.0);

        ret
    }
}

pub enum SenderError{
    Full,
    Closed
}