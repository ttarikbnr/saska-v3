#![recursion_limit = "512"]

mod socket;
mod conf;
mod client;
mod broker;
mod channel;
mod metrics;

pub mod plugin;

pub use plugin::Broker;
pub use plugin::DisconnectReason;
pub use mqtt::packet::{ConnectPacket, SubscribePacket, UnsubscribePacket, PublishPacket, Packet};
pub use mqtt::topic_name::TopicName;
pub use mqtt::packet::publish::QoSWithPacketIdentifier;
pub use mqtt::encodable::Encodable;
pub use config::Config;
pub use plugin::authz_result::AuthzResult;
pub use metrics::BrokerMetrics;

use futures::Future;

use tokio::runtime::Runtime;
use std::sync::Arc;

pub struct Saska<F, P> 
    where F: Future<Output=P> {
    //config                      : config::Config,
    pg_create_fn: F,
    runtime: Arc<Runtime>
}

impl<F, P> Saska<F, P>
    where P: plugin::PluginGenerator + 'static,
          F: Future<Output=P> + Send + 'static {
    pub fn new<S>(config_path: S,
                  pg_create_fn: F,
                  runtime: Arc<Runtime>) -> Self
        where S: AsRef<str> {

        conf::create_config(config_path);

        Self {
            pg_create_fn,
            runtime
        }
    }


    pub async fn run(self) {
        let localset = tokio::task::LocalSet::new();
        let tcp_listening_port = conf::get_tcp_listening_port();
        let websocket_listening_port = conf::get_ws_listening_port();


        localset.run_until(async move {
            let listener = socket::Listener::bind(tcp_listening_port, websocket_listening_port).await;
            let mut broker_task = broker::BrokerTask::new(self.pg_create_fn, self.runtime.clone());
            let mut client_spawner = broker_task.get_client_spawner();

            let f = async {
                loop {
                    let socket = listener.accept().await;
                    client_spawner.spawn_client(socket);
                }
            };
            tokio::select ! {
            _ = broker_task.run() => {}
            _ = f => {}
            }
        }).await;
    }
}
