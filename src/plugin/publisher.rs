use async_trait::async_trait;
use super::broker_requester::BrokerRequester;

#[cfg( feature = "sync-publisher")]
#[async_trait]
pub trait Publisher {
    async fn next(&mut self) -> (String, Vec<u8>);
}

#[cfg( not( feature = "sync-publisher"))]
#[async_trait(?Send)]
pub trait Publisher {
    async fn next(&mut self) -> (String, Vec<u8>);
}

pub struct PublisherTask <P>{
    publisher_plugin       : P,
    broker                 : BrokerRequester,  
}

impl <P> PublisherTask <P>
    where P: Publisher {
    pub fn new(publisher_plugin : P,
               broker           : BrokerRequester) -> Self {
        Self {
            publisher_plugin,
            broker
        }
    }
    pub async fn run(mut self) {
        loop {
            let (topic, payload) = self.publisher_plugin.next().await;
            self.broker.publish(topic, payload);
        }
    }
}
