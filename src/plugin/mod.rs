mod publisher;
pub(crate) mod plugin_requester;
pub mod authz_result;
mod broker_requester;

pub use publisher::Publisher;
pub use broker_requester::{BrokerRequester as Broker, PluginCommand};
pub use self::plugin_requester::DisconnectReason;
pub(crate) use publisher::PublisherTask;

use self::plugin_requester::{PluginRequest, PluginReply, PluginReplyTx};
use self::authz_result::AuthzResult;
use async_trait::async_trait;
use tokio::sync::{mpsc::UnboundedReceiver, oneshot};
use tokio::task::{LocalSet, spawn_local};
use tokio::runtime::Runtime;
use futures::{Future, StreamExt};
use mqtt::packet::{ConnectPacket, SubscribePacket, UnsubscribePacket};
use crate::metrics;





#[async_trait(?Send)]
pub trait PluginGenerator {
    type AuthenticationPlugin       : Authentication + 'static;
    type SubAuthorizationPlugin     : SubAuthorization + 'static;
    type PubAuthorizationPlugin     : PubAuthorization + 'static;
    type UnsubscribePlugin          : Unsubscribe + 'static;
    type DisconnectPlugin           : Disconnect + 'static;

    #[cfg( feature = "sync-publisher")]
    type PublisherPlugin            : Publisher + Send ;

    #[cfg( not( feature = "sync-publisher"))]
    type PublisherPlugin            : Publisher + 'static ;

    type Daemon                     : Future + 'static;

    async fn initialize(&mut self, broker: Broker);

    fn run_background_tasks(&mut self); 

    fn authentication(&mut self) -> Option<Self::AuthenticationPlugin> { None } 

    fn sub_authorization(&mut self) -> Option<Self::SubAuthorizationPlugin> { None } 

    fn pub_authorization(&mut self) -> Option<Self::PubAuthorizationPlugin> { None }
    
    fn unsubscribe(&mut self) -> Option<Self::UnsubscribePlugin> { None } 

    fn disconnection(&mut self) -> Option<Self::DisconnectPlugin> { None }

    fn publisher(&mut self) -> Option<Self::PublisherPlugin> { None }
}

#[async_trait]
pub trait Initialize {
    async fn initialize(self, broker: Broker);
}

#[async_trait(?Send)]
pub trait Authentication {
    async fn authentication(&self, client_id: usize, connect_packet: ConnectPacket) -> bool;
}

#[async_trait(?Send)]
pub trait SubAuthorization {
    async fn sub_authorization(&self, client_id: usize, subscribe_packet: SubscribePacket) -> AuthzResult;
}

#[async_trait(?Send)]
pub trait PubAuthorization {
    async fn pub_authorization(&self, client_id: usize) -> bool;
}

#[async_trait(?Send)]
pub trait Unsubscribe {
    async fn unsubscribe(&self, client_id: usize, unsubscribe_packet: UnsubscribePacket);
}

#[async_trait(?Send)]
pub trait Disconnect{
    async fn disconnection(&self, client_id: usize, disconnect_reason: DisconnectReason);
}


pub type PluginRequestRx = UnboundedReceiver<(usize, PluginRequest, oneshot::Sender<PluginReply>)>;

pub struct PluginTask <P, F>
    where F: Future<Output=P> {
    pg_create_fn                : F,
    broker                      : Broker,
    request_rx                  : PluginRequestRx
}

impl <P, F> PluginTask <P, F>
    where P: PluginGenerator + 'static,
          F: Future<Output=P> + Send + 'static  {

    pub fn new(pg_create_fn: F,
               broker: Broker,
               request_rx: PluginRequestRx) -> Self {
        Self {
            pg_create_fn,
            broker,
            request_rx
        }
    }

    pub fn spawn_plugin_thread(self, runtime: std::sync::Arc<Runtime>) {
        let _ = std::thread::Builder::new()
                              .name("plugin-thread".to_owned())
                              .spawn(move || {
                                    // Execute the future, blocking the current thread until completion.
                        
                                    let localset = LocalSet::new();
                                    localset.block_on( &runtime,self.run());
                                });
    }

    async fn run(self) {
        let mut plugin_generator = self.pg_create_fn.await;
        plugin_generator.initialize(self.broker.clone()).await;

        spawn_publisher_task(self.broker.clone(), plugin_generator.publisher());

        // Get and run background tasks if any exists
        plugin_generator.run_background_tasks();
        
        let authentication_plugin    = plugin_generator.authentication().into_rc();
        let sub_authorization_plugin = plugin_generator.sub_authorization().into_rc();
        let pub_authorization_plugin = plugin_generator.pub_authorization().into_rc();
        let unsubscribe_plugin       = plugin_generator.unsubscribe().into_rc();
        let disconnection_plugin     = plugin_generator.disconnection().into_rc();

        let mut request_rx = self.request_rx.fuse();

        loop {
            let (client_id, request, reply_tx) = request_rx.select_next_some().await;

            crate::increase_counter!(metrics::PLUGIN_REQUESTS);

            match request {
                PluginRequest::Authentication(connect_packet) => {
                    if let Some(authn_plugin) = authentication_plugin.clone(){ 

                        spawn_authn_handler(authn_plugin, client_id, connect_packet, reply_tx);

                    } else {
                        let reply = PluginReply::Authentication(true);
                        let _ = reply_tx.send(reply);
                    }
                }
                PluginRequest::SubAuthorization(subscribe_packet) => {
                    use mqtt::packet::Packet;
                    if let Some(authz_plugin) = sub_authorization_plugin.clone() {

                        spawn_sub_authz_handler(authz_plugin,
                                                client_id,
                                                subscribe_packet,
                                                reply_tx);

                    } else {

                        let topic_length = subscribe_packet.payload_ref()
                                                           .subscribes()
                                                           .len();

                        let all_false = AuthzResult::all_false(topic_length);
                        let reply = PluginReply::SubAuthorization(all_false);
                        let _ = reply_tx.send(reply);
                    }
                }
                PluginRequest::Unsubscribe(unsubscribe_packet) => {

                    if let Some(unsubscribe_plugin) = unsubscribe_plugin.clone() {
                        let unsubscribe_fut = async move {
                            unsubscribe_plugin.unsubscribe(client_id, unsubscribe_packet).await;
                        };
                        spawn_local(unsubscribe_fut);
                    }

                }
                PluginRequest::PubAuthorization => {

                    if let Some(authz_plugin)  = pub_authorization_plugin.clone() {

                        spawn_pub_authz_handler(authz_plugin,
                                                client_id,
                                                reply_tx);

                    } else {
                        let reply = PluginReply::PubAuthorization(true);
                        let _ = reply_tx.send(reply);
                    }

                }
                PluginRequest::Disconnection(disconnect_reason) => {

                    if let Some(disc_plugin) = disconnection_plugin.clone() {
                        spawn_disconnection_handler(disc_plugin,
                                                    client_id,
                                                    disconnect_reason,
                                                    reply_tx);
                    } else {
                        let _ = reply_tx.send(PluginReply::Disconnection);
                    }

                }
            }
        }
    }
}




#[inline(always)]
fn spawn_authn_handler<P>(authn_plugin     : Rc<P>,
                          client_id        : usize,
                          connect_packet   : ConnectPacket,
                          reply_tx         : PluginReplyTx)
    where P: Authentication + 'static {

    let authentication_fut = async move {
        let reply = authn_plugin.authentication(client_id, connect_packet).await;
        let authenticated = PluginReply::Authentication(reply);
        let _ = reply_tx.send(authenticated);
    };

    spawn_local(authentication_fut);
}



#[inline(always)]
fn spawn_sub_authz_handler<P>(authz_plugin     : Rc<P>,
                              client_id        : usize,
                              subscribe_packet : SubscribePacket,
                              reply_tx         : PluginReplyTx)
    where P: SubAuthorization + 'static {

    let authorization_fut = async move {
        let reply = authz_plugin.sub_authorization(client_id, subscribe_packet).await;
        let authorized = PluginReply::SubAuthorization(reply);
        let _ = reply_tx.send(authorized);
    };

    spawn_local(authorization_fut);
}



#[inline(always)]
fn spawn_pub_authz_handler<P>(authz_plugin     : Rc<P>,
                              client_id        : usize,
                              reply_tx         : PluginReplyTx)
    where P: PubAuthorization + 'static {

    let authorization_fut = async move {
        let reply = authz_plugin.pub_authorization(client_id).await;
        let authorized = PluginReply::PubAuthorization(reply);
        let _ = reply_tx.send(authorized);
    };

    spawn_local(authorization_fut);
}



#[inline(always)]
fn spawn_disconnection_handler<P>(disc_plugin : Rc<P>,
                                  client_id   : usize,
                                  reason      : DisconnectReason,
                                  reply_tx    : PluginReplyTx)
    where P: Disconnect + 'static {

    let disconnection_fut = async move {
        disc_plugin.disconnection(client_id, reason).await;
        let _ = reply_tx.send(PluginReply::Disconnection);
    };

    spawn_local(disconnection_fut);    
}




#[cfg(not( feature = "sync-publisher"))]
fn spawn_publisher_task<T>(broker: Broker, publisher_plugin: Option<T>)
    where T: Publisher + 'static {


    spawn_local(async move {
        if let Some(publisher_plugin) = publisher_plugin {
            let publisher_task = PublisherTask::new(publisher_plugin, broker);
            publisher_task.run().await
        }
    });
}



#[cfg( feature = "sync-publisher")]
fn spawn_publisher_task<T>(broker: Broker, publisher_plugin: Option<T>)
    where T: Publisher + Send + 'static {

    tokio::task::spawn(async move {
        if let Some(publisher_plugin) = publisher_plugin {
            let publisher_task = PublisherTask::new(publisher_plugin, broker);
            publisher_task.run().await
        }
    });
}


use std::rc::Rc;

pub trait IntoRc<T> {
    fn into_rc(self) -> Option<Rc<T>>;
}

impl <T: 'static> IntoRc<T> for Option<T> {
    fn into_rc(self) -> Option<Rc<T>> {
        self.map(Rc::new)
    }
}