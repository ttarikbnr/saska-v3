use bitvec::array::BitArray;
use bitvec::order::Lsb0;



enum Results {
    BitArray(usize, BitArray<Lsb0, u64>), // when topic count is under 64 this variant will be used
    BitVec(Vec<bool>)
}

impl Results {
    #[inline(always)]
    fn new(size: usize) -> Self {
        if size > 64 {
            Self::BitVec(Vec::with_capacity(size))
        } else {
            Self::BitArray(size, BitArray::zeroed())
        }
    }

    #[inline(always)]
    fn all_false(size: usize) -> Self {
        if size > 64 {
            Self::BitVec(vec![false; size as _])
        } else {
            Self::BitArray(size, BitArray::zeroed())
        }
    }

    #[inline(always)]
    fn all_true(size: usize) -> Self {
        if size > 64 {
            Self::BitVec(vec![true; size as _])
        } else {
            Self::BitArray(size, !BitArray::zeroed())
        }
    }



    #[inline(always)]
    fn set(&mut self, index: usize, value: bool) {
        match self {
            Self::BitVec(bitvec) => { bitvec.push(value) }
            Self::BitArray(_, bitarr) => { bitarr.set(index, value) }
        }
    }
}

pub struct AuthzResult {
    set_index   : usize,
    results     : Results,
}

impl AuthzResult {
    #[inline(always)]
    pub fn new(topic_count: usize) -> Self {
        let results = Results::new(topic_count as _);
        Self {
            set_index   : 0,
            results
        }
    }

    #[inline(always)]
    pub fn all_false(topic_count: usize) -> Self{
        Self {
            set_index   : 0,
            results     : Results::all_false(topic_count as _)
        }
    }

    #[inline(always)]
    pub fn all_true(topic_count: usize) -> Self{
        Self {
            set_index   : 0,
            results     : Results::all_true(topic_count as _)
        }
    }

    #[inline(always)]
    pub fn push(&mut self, value: bool) {
        self.results.set(self.set_index, value);
        self.set_index += 1;
    }

    #[inline(always)]
    pub fn iter(&self) -> AuthzResultIterator {
        AuthzResultIterator::new(&self.results)
    }
}


pub struct AuthzResultIterator<'a> {
    idx     : usize,
    results : &'a Results
}

impl <'a> AuthzResultIterator<'a> {
    fn new(results: &'a Results) -> Self {
        Self {
            idx: 0,
            results
        }
    }
}

impl <'a> Iterator for AuthzResultIterator<'a> {
    type Item = bool;

    fn next(&mut self) -> Option<Self::Item> {

        match self.results {
            Results::BitArray(size, bitarr) => {
                if self.idx < *size as _ {
                    let ret = bitarr.get(self.idx as usize).cloned();
                    self.idx += 1;
                    ret
                } else {
                    None
                }
            }
            Results::BitVec(bitvec) => {
                let ret = bitvec.get(self.idx).cloned();
                self.idx += 1;
                ret
            }
        }
    }
}





#[test]
fn test_topic_count_10() {
    let topic_count = 10;
    let mut authz_result = AuthzResult::new(topic_count);
    
    for _ in 0..topic_count / 2 {
        authz_result.push(true);
    }

    for _ in 0..topic_count / 2 {
        authz_result.push(false);
    }

    let mut iter = authz_result.iter();

    for _ in 0..topic_count / 2 {
        assert_eq!(iter.next(), Some(true));
    }

    for _ in 0..topic_count / 2 {
        assert_eq!(iter.next(), Some(false));
    }

    assert_eq!(iter.next(), None)
}

#[test]
fn test_topic_count_100() {
    let topic_count = 100;
    let mut authz_result = AuthzResult::new(topic_count);
    
    for _ in 0..topic_count / 2 {
        authz_result.push(true);
    }

    for _ in 0..topic_count / 2  {
        authz_result.push(false);
    }

    let mut iter = authz_result.iter();

    for _ in 0..topic_count / 2  {
        assert_eq!(iter.next(), Some(true));
    }

    for _ in 0..topic_count / 2  {
        assert_eq!(iter.next(), Some(false));
    }

    assert_eq!(iter.next(), None)
}


#[test]
fn test_all_false_10() {
    let topic_count  = 10;
    let authz_result = AuthzResult::all_false(topic_count);

    let mut iter = authz_result.iter();

    for _ in 0..topic_count {
        assert_eq!(iter.next(), Some(false));
    }

    assert_eq!(iter.next(), None)
}


#[test]
fn test_all_false_100() {
    let topic_count  = 100;
    let authz_result = AuthzResult::all_false(topic_count);

    let mut iter = authz_result.iter();

    for _ in 0..topic_count {
        assert_eq!(iter.next(), Some(false));
    }

    assert_eq!(iter.next(), None)
}


#[test]
fn test_all_true_40() {
    let topic_count  = 40;
    let authz_result = AuthzResult::all_true(topic_count);

    let mut iter = authz_result.iter();

    for _ in 0..topic_count {
        assert_eq!(iter.next(), Some(true));
    }

    assert_eq!(iter.next(), None)
}

#[test]
fn test_all_true_100() {
    let topic_count  = 100;
    let authz_result = AuthzResult::all_true(topic_count);

    let mut iter = authz_result.iter();

    for _ in 0..topic_count {
        assert_eq!(iter.next(), Some(true));
    }

    assert_eq!(iter.next(), None)
}