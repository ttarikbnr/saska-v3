use tokio::sync::mpsc::UnboundedSender;

pub enum DisconnectReason {
    Multiconnect,
}

// Enum represenation of orders that plugin gives to broker
pub enum PluginCommand {
    Subscribe{
        client_id   : usize,
        topic       : String, 
    },
    Publish {
        topic       : String,
        packet      : Vec<u8>
    },
    PublishToClient {
        client_id   : usize,
        topic       : String,
        packet      : Vec<u8>
    },
    SendToClient {
        client_id   : usize,
        packet      : Vec<u8>
    },
    ForceDisconnect {
        client_id           : usize,
        disconnect_reason   : DisconnectReason,
    }
}


/// Requester mechanism given to plugin callbacks when it needs to broker do some action
#[derive(Clone)]
pub struct BrokerRequester {
    to_broker_task : UnboundedSender<PluginCommand>,
}

impl BrokerRequester {
    pub(crate) fn new(to_broker_task: UnboundedSender<PluginCommand>) -> Self {
        Self {
            to_broker_task
        }
    }

    /// Subscribe to topic for given client
    pub fn subscribe(&self, client_id: usize, topic: String) {
        let msg = PluginCommand::Subscribe {client_id, topic};
        let _ = self.to_broker_task.send(msg)
            .map_err(log_send_err);
    }

    /// Publish to all clients which are subscribed to given topic
    pub fn publish(&self, topic: String, packet: Vec<u8>) {
        let msg = PluginCommand::Publish {topic, packet};
        let _ = self.to_broker_task.send(msg)
            .map_err(log_send_err);
    }

    /// Publish packet to given client. This method will send the packet whether the client is
    /// subscribed or not.
    pub fn publish_to_client(&self, client_id : usize, topic: String, packet: Vec<u8>) {
        let msg = PluginCommand::PublishToClient {client_id, topic, packet};
        let _ = self.to_broker_task.send(msg)
            .map_err(log_send_err);
    }

    pub fn send_to_client_raw(&self, client_id : usize, packet: Vec<u8>) {
        let msg = PluginCommand::SendToClient {client_id, packet};
        let _ = self.to_broker_task.send(msg)
            .map_err(log_send_err);
    }

    /// This method should be called for old connection when the same client establises an new connection
    pub async fn multiconnect(&self, client_id : usize ) {
        let disconnect_reason = DisconnectReason::Multiconnect;
        let msg = PluginCommand::ForceDisconnect {client_id, disconnect_reason};
        let _ = self.to_broker_task.send(msg)
            .map_err(log_send_err);
    }
}

fn log_send_err<E>(_err: E) {
    log::error!("Couldn't send to command to broker task.");
}