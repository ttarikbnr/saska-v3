use tokio::sync::mpsc::UnboundedSender;
use tokio::sync::oneshot;
use mqtt::packet::ConnectPacket;
use mqtt::packet::SubscribePacket;
use mqtt::packet::UnsubscribePacket;
use super::authz_result::AuthzResult;




pub type PluginReplyTx = oneshot::Sender<PluginReply>;
pub type PluginRequestTx = UnboundedSender<(usize, PluginRequest, PluginReplyTx)>;

pub enum DisconnectReason {
    ClientDisconnected,
    KeepaliveExpired,
    PluginRequested,
}

pub enum PluginRequest {
    Authentication(ConnectPacket),
    SubAuthorization(SubscribePacket),
    Unsubscribe(UnsubscribePacket),
    PubAuthorization,
    Disconnection(DisconnectReason),
}

pub enum PluginReply {
    Authentication(bool),
    SubAuthorization(AuthzResult),
    PubAuthorization(bool),
    Disconnection
}


#[derive(Clone)]
pub struct PluginRequester{
    plugin_request_tx: PluginRequestTx,
}

impl PluginRequester {
    pub fn new(plugin_request_tx: PluginRequestTx) -> Self {
        Self {
            plugin_request_tx
        }
    }

    pub async fn authentication(&self, client_id: usize, connect_packet: ConnectPacket) -> Result<bool, ()> {
        let req = PluginRequest::Authentication(connect_packet);
        let (rep_tx, rep_rx) = oneshot::channel();

        if let Err(err) = self.plugin_request_tx.send((client_id, req, rep_tx)) {
            log::error!("Couldn't send plugin request! {}", err);
        }

        match rep_rx.await {
            Ok(rep) => {
                match rep {
                    PluginReply::Authentication(authenticated) => Ok(authenticated),
                    _ => {
                        Err(())
                    }
                }
            },
            Err(err) => {
                log::error!("Plugin reply error {}", err);
                Err(()) 
            }
        }
    }

    pub async fn sub_authorization(&self, client_id: usize, subscribe_packet: SubscribePacket) -> Result<AuthzResult, ()>{
        let req = PluginRequest::SubAuthorization(subscribe_packet);
        let (rep_tx, rep_rx) = oneshot::channel();
        if let Err(err) = self.plugin_request_tx.send((client_id, req, rep_tx)) {
            log::error!("Couldn't send plugin request! {}", err);
        }

        match rep_rx.await {
            Ok(rep) => {
                match rep {
                    PluginReply::SubAuthorization(authorized) => Ok(authorized),
                    _ => {
                        Err(()) 
                    }
                }
            },
            Err(err) => {
                log::error!("Plugin reply error {}", err);
                Err(())
            }
        }
    }

    pub async fn unsubscribe(&self, client_id: usize, unsubscribe_packet: UnsubscribePacket) {
        let req = PluginRequest::Unsubscribe(unsubscribe_packet);
        let (rep_tx, _rep_rx) = oneshot::channel();
        if let Err(err) = self.plugin_request_tx.send((client_id, req, rep_tx)) {
            log::error!("Couldn't send plugin request! {}", err);
        }
    }

    pub async fn disconnection(&self, client_id: usize, disconnect_reason: DisconnectReason) -> Result<() ,()> {
        let req = PluginRequest::Disconnection(disconnect_reason);
        let (rep_tx, rep_rx) = oneshot::channel();
        if let Err(err) = self.plugin_request_tx.send((client_id, req, rep_tx)) {
            log::error!("Couldn't send plugin request! {}", err);
            Ok(())
        } else {
            rep_rx.await
                .map(|_| ())
                .map_err(|_| ())
        }
    }
}
