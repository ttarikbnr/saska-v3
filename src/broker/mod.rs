mod subscription;
mod channels;
mod client_event;
mod broker_task;


pub(crate) mod client_spawner;
pub use broker_task::BrokerTask;
pub use channels::BrokerCommand;
pub use client_event::ClientEvent;

use subscription::{Subscription, HashMapSub};
use std::collections::HashSet;
use crate::metrics;
use channels::Channels;
use std::sync::Arc;
use std::rc::Rc;
use std::cell::RefCell;

type Subscriptions = Rc<RefCell<HashMapSub>>;

pub(self) struct Broker {
    subscriptions       : Subscriptions,
    channels            : Channels,
    waiting_publishes   : HashSet<usize>,
}

impl Broker { 

    pub fn new(subscriptions    : Subscriptions,
               channels         : Channels ) -> Self {

        Self {
            channels,
            subscriptions,
            waiting_publishes: HashSet::with_capacity(1024*8),
        }
    }

    pub async fn subscribe(&self, client_id: usize, topic: String) {
        self.subscriptions
            .borrow_mut()
            .subscribe(client_id, &*topic);
    }

    pub fn publish(&mut self, topic: String, payload: Vec<u8>) {
        let packet = crate::client::create_and_serialize_publish_packet(topic.clone(), payload);
        let packet_sync = Arc::new(packet);
        let now = now();
        let mut gonna_remove = vec![];

        for client_id in self.subscriptions
                             .borrow()
                             .subscribers(&topic){
            if !self.channels.send(*client_id, packet_sync.clone(), now) {
                gonna_remove.push(*client_id);
            } else {
                self.waiting_publishes.insert(*client_id);
            }
        }

        let publish_cnt = self.waiting_publishes.len();
        crate::increase_counter_by!(metrics::PUBLISH_COUNT, publish_cnt);

        for client_id in gonna_remove {
            self.subscriptions.borrow_mut()
                .unsubscribe(client_id, &*topic);
            self.channels.remove_channels(client_id);
        }
    }

    pub fn notify_all(&mut self)  {
        let now = now();
        for id in self.waiting_publishes.iter() {
            self.channels.notify(*id, now);
        }
        self.waiting_publishes.clear();
    }

    pub fn publish_to_client(&mut self, client_id : usize, topic: String, payload: Vec<u8>) {
        let packet= crate::client::create_and_serialize_publish_packet(topic, payload);
        let packet_sync = Arc::new(packet);
        self.channels.send(client_id, packet_sync, now());
        self.waiting_publishes.insert(client_id);
    }

    pub fn send_to_client(&mut self, client_id : usize, payload: Vec<u8>) {
        let packet_sync = Arc::new(payload);
        self.channels.send(client_id, packet_sync, now());
        self.waiting_publishes.insert(client_id);
    }

    pub fn force_disconnect_client(&self, client_id : usize) {
        self.channels.force_disconnect(client_id);
    }
}

fn now() -> u64 {
    use std::time::{SystemTime, UNIX_EPOCH};
    SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_millis() as _
}
