use tokio::sync::mpsc::UnboundedReceiver;
use super::channels::Channels;
use std::cell::RefCell;
use std::rc::Rc;
use super::subscription::Subscription;

type Subscriptions<S> = Rc<RefCell<S>>;

pub enum ClientEvent {
    #[allow(dead_code)]
    Connected(usize),
    Subscribed(usize, Vec<String>),
    Unsubscribed(usize, Vec<String>),
    Disconnected(usize),
    Published(String, Vec<u8>),
}

// Task that handles client events and stores all clients subscriptions on shared state
pub struct ClientEventTask <S>{
    // Receives client events
    client_event_rx : UnboundedReceiver<ClientEvent>,

    channels        : Channels,
    subscriptions   : Subscriptions<S>,
}

impl <S>ClientEventTask<S>
    where S: Subscription {

    pub fn new(subscriptions    : Subscriptions<S>,
               client_event_rx  : UnboundedReceiver<ClientEvent>,
               channels         : Channels) -> Self {
        Self {
            client_event_rx,
            channels,
            subscriptions,
        }
    }

    pub async fn run(mut self) {
        while let Some(event) = self.client_event_rx.recv().await {
            match event {
                ClientEvent::Connected(_client_id) => {
                    // info!("New client with id {} is connected!", client_id);
                },

                ClientEvent::Subscribed(client_id, topics) => {
                    for topic in topics {
                        self.subscriptions.borrow_mut().subscribe(client_id, &*topic);
                    }
                },
                ClientEvent::Unsubscribed(client_id, topics) => {
                    for topic in topics {
                        self.subscriptions.borrow_mut().unsubscribe(client_id, &*topic);
                    }
                },
                ClientEvent::Disconnected(client_id) => {

                    self.subscriptions.borrow_mut().remove_client(client_id);

                    self.channels.remove_channels(client_id);
                },

                ClientEvent::Published(_topic, _data) => {
                    // Note : Client can't publish yet
                } 
            }
        }

        log::error!("Client command channel is broken!");
    }
}

impl <S>Drop for ClientEventTask<S> {
    fn drop(&mut self) {
        log::error!("ClientEventTask is dropped!");
    }
}