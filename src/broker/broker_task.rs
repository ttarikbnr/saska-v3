use super::client_event::ClientEventTask;
use super::channels::Channels;
use super::client_spawner::ClientSpawner;
use tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver};
use tokio::runtime::Runtime;
use tokio::time::interval;
use futures::{StreamExt, Future};
use crate::plugin::{Broker as BrokerRequester, PluginCommand,
    PluginGenerator, PluginTask, plugin_requester::PluginRequester};
use std::sync::Arc;
use std::rc::Rc;
use std::cell::RefCell;
use std::time::Duration;
use super::Broker;
use super::subscription::HashMapSub;
use crate::conf;

pub struct BrokerTask {
    #[allow(unused)]
    channels                : Channels,

    #[allow(unused)]
    plugin_requester        : PluginRequester,

    from_plugin_rx          : UnboundedReceiver<PluginCommand>,
    client_spawner          : Option<ClientSpawner>,
    broker                  : Broker,
}

impl BrokerTask {
    #[allow(unused_mut)]
    pub fn new<F, P>(mut pg_create_fn: F, runtime: Arc<Runtime>) -> Self 
        where P: PluginGenerator +  'static,
              F: Future<Output=P> + Send + 'static{

        // from clients to broker
        // clients send events through this channel
        let (client_action_tx, client_action_rx) = unbounded_channel();

        // from clients to plugin
        let (plugin_request_tx, plugin_request_rx) = unbounded_channel();

        // from plugin to broker channel
        // plugin send commands through this channel
        let (from_plugin_tx, from_plugin_rx) = unbounded_channel();

        // broker to clients
        let channels = Channels::new();
        

        let subscriptions = Rc::new(RefCell::new(HashMapSub::new()));

        let client_event_task = ClientEventTask::new(
            subscriptions.clone(),
            client_action_rx,
            channels.clone()
        );

        tokio::task::spawn_local(client_event_task.run());

        let broker = Broker::new(subscriptions,
                                 channels.clone());

        let plugin_requester = PluginRequester::new(plugin_request_tx);

        let broker_requester = BrokerRequester::new(from_plugin_tx);

        let plugin_task = PluginTask::new(pg_create_fn,
                                          broker_requester,
                                          plugin_request_rx);

        plugin_task.spawn_plugin_thread(runtime);


        let client_spawner = ClientSpawner::new(channels.clone(),
                                                client_action_tx,
                                                plugin_requester.clone());

        // Spawn client event task


        Self {
            channels,
            plugin_requester,
            from_plugin_rx,
            client_spawner: Some(client_spawner),
            broker,
        }
    }

    pub fn get_client_spawner(&mut self) -> ClientSpawner {
        self.client_spawner.take().unwrap()
    }


    /// Run commands received from plugin
    pub async fn run(mut self) {
        let mut from_plugin_rx = self.from_plugin_rx.fuse();
        let interval_period = Duration::from_millis(conf::get_data_flush_period_ms());
        let mut interval = interval(interval_period).fuse();

        loop {
            futures::select_biased!(
                plugin_command = from_plugin_rx.select_next_some() => {
                   match plugin_command {
                        PluginCommand::Subscribe{
                            client_id,
                            topic,
                        } => {
                            self.broker.subscribe(client_id, topic).await;
                        }
                        PluginCommand::Publish {
                            topic,
                            packet,
                        } => {
                            self.broker.publish(topic, packet);
                        }
                        PluginCommand::PublishToClient {
                            client_id,
                            topic,
                            packet,
                        } => {
                            self.broker.publish_to_client(client_id, topic, packet);
                        }
                        PluginCommand::SendToClient {
                            client_id,
                            packet,
                        } => {
                            self.broker.send_to_client(client_id, packet);
                        }

                        PluginCommand::ForceDisconnect {
                            client_id,
                            ..
                        } => {
                            self.broker.force_disconnect_client(client_id); 
                        }
                   } 
                }
                _ = interval.select_next_some() => {
                    self.broker.notify_all();
                }
                complete => {
                    log::error!("Unreachable");
                }
            );
        }
        
    }
}
