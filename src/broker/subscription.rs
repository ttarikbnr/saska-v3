use std::collections::{HashMap, HashSet};

const EMPTY_USER_LIST : [usize; 0] = [];

fn empty_user_list() -> &'static [usize] {
    &EMPTY_USER_LIST[..]
}

pub trait Subscription {
    fn subscribe(&mut self, client_id: usize, topic: &str);
    fn unsubscribe(&mut self, client_id: usize, topic: &str);
    fn remove_client(&mut self, client_id: usize);
    fn subscribers(&self, topic: &str) -> &[usize];
}

struct Subscribers {
    set : HashSet<usize>,
    vec : Vec<usize>,
}

impl Subscribers {
    pub fn new_with_client(client_id: usize) -> Self {
        let mut set = HashSet::new();
        let vec = vec![client_id];

        set.insert(client_id);

        Self {
            set,
            vec
        }
    }

    pub fn insert(&mut self, client_id: usize) {
        if self.set.insert(client_id) {
            self.vec.push(client_id);
        }
    }

    pub fn remove(&mut self, client_id: usize) {
        if self.set.remove(&client_id) {
            if let Some(index) = self.vec.iter().position(|x| *x == client_id) {
                self.vec.remove(index);
            }
        }
    }
}

pub struct HashMapSub {
    subscriptions : HashMap<String, Subscribers>,
    clients       : HashMap<usize,  HashSet<String>>
} 

impl HashMapSub{
    pub fn new() -> Self {
        Self {
            subscriptions: HashMap::new(),
            clients      : HashMap::new()
        }
    }
}

impl Subscription for HashMapSub {
    fn subscribe(&mut self,
                 client_id: usize,
                 topic: &str) {
        if let Some(subscribers) = self.subscriptions
                                       .get_mut(topic) {
            subscribers.insert(client_id);
            self.clients.entry(client_id).or_insert_with(HashSet::new).insert(topic.to_string());
        } else {
            let subscribers = Subscribers::new_with_client(client_id);
            self.subscriptions.insert(topic.to_string(), subscribers);
            self.clients.entry(client_id).or_insert_with(HashSet::new).insert(topic.to_string());
        }
    }

    fn unsubscribe(&mut self,
                   client_id: usize,
                   topic: &str) {
        if let Some(subscribers) = self.subscriptions
                                         .get_mut(topic) {
            subscribers.remove(client_id);
            self.clients.get_mut(&client_id).map(|topics| topics.remove(topic));
        }
    }

    fn remove_client(&mut self, client_id: usize) {
        if let Some(client_topics) = self.clients.remove(&client_id) {
            for topic in client_topics.iter() {
                if let Some(subscribers) = self.subscriptions
                                                .get_mut(topic) {
                    subscribers.remove(client_id);
                }
            }
        }
    }

    fn subscribers(&self, topic: &str) -> &[usize] {
        self.subscriptions
            .get(topic)
            .map(|subscribers| {
                subscribers.vec.as_ref()
            })
            .unwrap_or_else(|| empty_user_list())
    }
}


