use super::channels::Channels;
use crate::client::{Client, ConnectFuture, ConnectError};
use crate::plugin::plugin_requester::PluginRequester;
use crate::broker::ClientEvent;
use crate::socket::Socket;
#[allow(unused)]
use futures::stream::{FuturesOrdered, StreamExt, FusedStream};
use tokio::sync::mpsc::{channel, unbounded_channel, UnboundedReceiver, UnboundedSender};
use std::time::Duration;
use std::mem::discriminant;
use crate::conf;

struct ClientSpawnerTask {
    channels            : Channels,
    client_action_tx    : UnboundedSender<ClientEvent>,
    plugin_requester    : PluginRequester,
    socket_recv         : UnboundedReceiver<Socket>,
    connect_fut_timeout : Duration,
    command_channel_cap : usize,
}

impl ClientSpawnerTask {
    fn new(
        channels            : Channels,
        client_action_tx    : UnboundedSender<ClientEvent>,
        plugin_requester    : PluginRequester,
        socket_recv         : UnboundedReceiver<Socket>,
    ) -> Self {
        let connect_fut_timeout = Duration::from_secs(conf::get_connect_fut_timeout());
        let command_channel_cap = conf::get_command_channel_capacity();

        Self {
            channels,
            client_action_tx,
            plugin_requester,
            socket_recv,
            connect_fut_timeout,
            command_channel_cap
        }
    }

    async fn run(self) {
        let plugin_requester = self.plugin_requester;
        let mut channels = self.channels.clone();
        let client_action_tx = self.client_action_tx;
        let mut socket_recv = self.socket_recv.fuse();
        let connect_fut_timeout = self.connect_fut_timeout;
        let command_channel_cap = self.command_channel_cap;

        loop {
            let socket = socket_recv.select_next_some().await;
            let client_id = channels.reserve_channels();

            let connect_fut = ConnectFuture::new(
                client_id,
                plugin_requester.clone(),
                socket,
                connect_fut_timeout,
            );

            tokio::task::spawn_local({
                let channels_cl = channels.clone();
                let client_action_tx = client_action_tx.clone();

                async move {
                    // Wait for client to connect
                    let connect_res = connect_fut.connect().await;

                    match connect_res {
                        Ok(client_params) => { // Connection is successful
                            let (data_channel_tx, data_channel_rx) = crate::channel::channel();
                            let (command_channel_tx, command_channel_rx) = channel(command_channel_cap);

                            channels_cl.add_new_client_channels(
                                client_params.client_id,
                                command_channel_tx,
                                data_channel_tx,
                            );

                            let client = Client::new(
                                client_params.client_id,
                                data_channel_rx,
                                command_channel_rx,
                                client_action_tx,
                                client_params.socket,
                                client_params.plugin_requester,
                                client_params.keep_alive,
                            );
    
                            // Spawn the client
                            tokio::spawn(client.run());
                        }
                        Err((_reserved_client_id, err)) => {
                            if discriminant(&ConnectError::AuthenticationPluginError) != discriminant(&err) {
                                log::error!("Client couldn't connect. {}", err);
                            }
                        }
                    }
                }
            });
        }
    }
}

pub struct ClientSpawner {
    socket_sender: UnboundedSender<Socket>,
}

impl ClientSpawner {
    pub fn new(
        channels: Channels,
        client_action_tx: UnboundedSender<ClientEvent>,
        plugin_requester: PluginRequester,
    ) -> Self {
        let (sender, receiver) = unbounded_channel();
        let client_spawner_task =
            ClientSpawnerTask::new(channels, client_action_tx, plugin_requester, receiver);

        tokio::task::spawn_local(client_spawner_task.run());

        Self {
            socket_sender: sender,
        }
    }

    pub fn spawn_client(&mut self, socket: Socket) {
        if let Err(err) = self.socket_sender.send(socket) {
            log::error!("Couldn't spawn client.\n{}", err);
        }
    }
}
