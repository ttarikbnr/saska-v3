

use std::rc::Rc;
use std::cell::RefCell;
use std::sync::Arc;
use tokio::sync::mpsc;
use tokio::sync::mpsc::error::TrySendError;
use crate::channel::{Sender, SenderError};
use std::collections::HashMap;

pub enum BrokerCommand {
    #[allow(unused)]
    Publish(String, Arc<Vec<u8>>),
    ForceDisconnect,
}

struct Senders {
    // Broker to client command sender for client that successfully connected
    command_sender  : mpsc::Sender<BrokerCommand>,

    // Broker to client data sender for client that successfully connected
    data_sender     : Sender<Arc<Vec<u8>>>,
}

impl Senders {
    fn notify(&self, millis: u64) {
        self.data_sender.flush(millis);
    }

    fn publish(&self,
               publish_enc    : Arc<Vec<u8>>,
               millis         : u64) -> bool {
        !matches!(self.data_sender.send((millis, publish_enc)), Err(SenderError::Closed))
    }

    fn send_command(&self, command: BrokerCommand) -> Result<(), TrySendError<BrokerCommand>> {
        self.command_sender.try_send(command)
    }
}


#[derive(Clone)]
pub struct Channels {
    client_id_cursor: usize,
    senders     : Rc<RefCell<HashMap<usize, Senders>>>

}

impl Channels {
    pub fn new() -> Self {
        Self {
            client_id_cursor: 0,
            senders : Rc::new(RefCell::new(HashMap::new())),
        }
    }

    // Returns slab index
    pub fn reserve_channels(&mut self) -> usize {
        // self.senders.borrow_mut().insert(None);
        let ret = self.client_id_cursor;
        self.client_id_cursor += 1;
        ret
    }

    // Remove without calling inner types destructor
    pub fn remove_channels(&mut self, client_id: usize) {
        self.senders
            .borrow_mut()
            .remove(&client_id);
    }

    pub fn add_new_client_channels(&self,
                                    client_id         : usize,
                                    command_sender    : mpsc::Sender<BrokerCommand>,
                                    data_sender       : Sender<Arc<Vec<u8>>>){
        self.senders.borrow_mut().insert(client_id, Senders {
            command_sender,
            data_sender,
        });

    }


    // Returns true if publish is successful
    // Returns false if there is no such client.
    pub fn send(&self,
                client_id      : usize,
                publish_enc    : Arc<Vec<u8>>,
                millis         : u64) -> bool {
        let mut borrowed = self.senders.borrow_mut();
        if let Some(sender) = borrowed.get_mut(&client_id) {
            sender.publish(publish_enc, millis)
        } else {
            log::debug!("Couldn't publish to client with id {}. There is no such client.", client_id);
            false
        }
    }

    pub fn notify(&self,
                  client_id : usize,
                  millis         : u64) {
        if let Some(sender) = self.senders.borrow_mut().get_mut(&client_id) {
            sender.notify(millis)
        }
    }

    pub fn force_disconnect(&self, client_id: usize) {
        let dc = BrokerCommand::ForceDisconnect;

        let mut borrowed = self.senders.borrow_mut();

        if let Some(command_sender) = borrowed.get_mut(&client_id) {
            let send_result = command_sender.send_command(dc);
            if let Err(err) = send_result {
                log::debug!(
                    "Couldn't send disconnect command to client with id {}. {}",
                    client_id, err
                );
            }
        } else {
            log::debug!("Couldn't force disconnect the client with id {}. There is no such client.", client_id);
        }
    }
}
