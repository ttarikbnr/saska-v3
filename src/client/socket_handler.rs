use crate::conf;
use crate::socket::Socket;
use crate::metrics;
use mqtt::packet::VariablePacket;
use std::sync::Arc;
use tokio::sync::mpsc::{Receiver, Sender};
use futures::{StreamExt, FutureExt};

macro_rules! write {
    ($writer:expr, $buf:expr, $label:tt) => {
        if let Err(_) = $writer.write_all(&$buf).await {
            break $label;
        }
    };
}

pub struct SocketHandler {
    socket              : Socket,

    inner_recv          : Receiver<Vec<u8>>,

    from_broker_recv    : crate::channel::Receiver<Arc<Vec<u8>>>,

    packet_tx           : Sender<VariablePacket>,
}

impl SocketHandler {
    pub fn new(data_rx_outer     : crate::channel::Receiver<Arc<Vec<u8>>>,
                data_rx_inner    : Receiver<Vec<u8>>,
                socket           : Socket,
                packet_tx        : Sender<VariablePacket>) -> Self {
        Self {
            socket,
            inner_recv: data_rx_inner,
            from_broker_recv: data_rx_outer,
            packet_tx
        }
    }

    pub async fn run(self) {
        if conf::get_write_buffered() {
            self.run_buffered().await
        } else {
            self.run_unbuffered().await
        }
    }

    async fn run_buffered(self) {
        let mut socket = self.socket;
        let (mut reader,mut writer) = socket.split();
        let buffer_cap = conf::get_write_buffer_capacity();
        let mut write_buffer = Vec::with_capacity(buffer_cap);

        let mut data_rx_inner = self.inner_recv.fuse();
        let mut data_rx_outer = self.from_broker_recv.fuse();

        'outer: loop {
            futures::select_biased! {
                packet_res = reader.read().fuse() => {
                    match packet_res {
                        Ok(packet) => {
                            if let Err(err) = self.packet_tx.send(packet).await {
                                log::debug!("Couldn't send incoming packet to client message listener.\n{}", err);
                                break 'outer;
                            }
                        },
                        Err(_err) => {
                            break 'outer;
                        }
                    }
                }
                data = data_rx_inner.select_next_some() => {
                    write!(writer, data, 'outer);
                },
                data = data_rx_outer.select_next_some() => {
                    if data.len() > write_buffer.capacity() {
                        crate::increase_counter!(metrics::WRITE_COUNT_BIG);

                        write!(writer, data, 'outer);
                    } else {
                        write_buffer.extend_from_slice(&*data);
                    }

                    while let Some(data) = data_rx_outer.get_mut().recv() {
                        if data.len() > write_buffer.capacity() {
                            // First send buffered data
                            if !write_buffer.is_empty() {
                                crate::increase_counter!(metrics::WRITE_COUNT);
                                
                                write!(writer, write_buffer, 'outer);
                            }

                            unsafe { write_buffer.set_len(0) }

                            crate::increase_counter!(metrics::WRITE_COUNT_BIG);

                            write!(writer, data, 'outer);

                        } else if write_buffer.len() < buffer_cap {
                            write_buffer.extend_from_slice(&*data);
                        } else {
                            write_buffer.extend_from_slice(&*data);

                            crate::increase_counter!(metrics::WRITE_COUNT);

                            write!(writer, write_buffer, 'outer);
                            unsafe { write_buffer.set_len(0) } 
                        }
                    }

                    if !write_buffer.is_empty() {
                        crate::increase_counter!(metrics::WRITE_COUNT);
                        write!(writer, write_buffer, 'outer);
                    }
                    unsafe { write_buffer.set_len(0)};
                },
            }
        }

        writer.shutdown().await;
    }


    async fn run_unbuffered(self) {
        let mut socket = self.socket;
        let (mut reader,mut writer) = socket.split();

        let mut data_rx_inner = self.inner_recv.fuse();
        let mut data_rx_outer = self.from_broker_recv.fuse();

        'outer: loop {
            futures::select_biased! {
                packet_res = reader.read().fuse() => {
                    match packet_res {
                        Ok(packet) => {
                            if let Err(err) = self.packet_tx.send(packet).await {
                                log::debug!("Couldn't send incoming packet to client message listener.\n{}", err);
                                break 'outer;
                            }
                        },
                        Err(_err) => {
                            break 'outer;
                        }
                    }
                }
                data = data_rx_inner.select_next_some() => {
                    log::info!("Inner rx data : {}", data.len());
                    write!(writer, data, 'outer);
                },
                data = data_rx_outer.select_next_some() => {
                    log::info!("Outer rx data : {}", data.len());
                    write!(writer, data, 'outer);

                    while let Some(data) = data_rx_outer.get_mut().recv() {
                        log::info!("Outer rx data : {}", data.len());
                        write!(writer, data, 'outer);
                    }
                },
            }
        }

        writer.shutdown().await;
    }
}



    
    
