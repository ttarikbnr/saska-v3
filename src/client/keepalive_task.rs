use tokio::sync::RwLock;
use std::sync::Arc;
use std::time::{Instant, Duration};
use tokio::time::interval;
use futures::future::pending;


pub struct KeepaliveTask {
    period_sec      : Option<u64>,
    last_activity   : Arc<RwLock<Instant>>,
}


impl KeepaliveTask {
    pub fn new(period_sec               : Option<u64>,
               last_activity            : Arc<RwLock<Instant>>) -> Self {
        Self {
            period_sec : period_sec.map(|x| (x as f64 * 1.5) as u64),
            last_activity
        }
    }

    
    pub async fn run(self) {
        if let Some(period) = self.period_sec {
            let duration = Duration::from_secs(period);
            let mut interval = interval(duration);

            loop {
                let _ = interval.tick()
                                .await;

                let now = Instant::now();

                let last_activity = self.last_activity
                                        .read()
                                        .await;

                let elapsed_maybe = now.checked_duration_since(*last_activity); 
                
                if let Some(elapsed) = elapsed_maybe {
                    if elapsed.as_secs() > period {
                        return
                    }
                }
            }
        } else {
            pending::<()>().await
        }
    }
}
