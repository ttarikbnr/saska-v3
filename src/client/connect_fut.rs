use crate::socket::{Socket, SocketError};
use crate::plugin::plugin_requester::PluginRequester;
use std::{
    time::Duration,
    fmt::{self, Display, Formatter},
};
use mqtt::{
    packet::{VariablePacket, ConnackPacket},
    control::variable_header::ConnectReturnCode,
    encodable::Encodable,
};
use tokio::{
    time::timeout,
    time::error::Elapsed,
};


/// Task that runs when new socket connection established
/// Waits for the mqtt connect packet until the timeout expires
pub struct ConnectFuture {
    pub client_id           : usize,
    pub plugin_requester    : PluginRequester,
    pub socket              : Socket,
    timeout_dur             : Duration,
    pub keep_alive          : Option<u64>,
}

impl ConnectFuture {
    pub fn new(client_id        : usize,
               plugin_requester : PluginRequester,
               socket           : Socket,
               timeout_dur      : Duration) -> Self {

        Self {
            client_id,
            plugin_requester,
            socket,
            timeout_dur,
            keep_alive: None,
        }
    }

    pub async fn connect(mut self) -> Result<ConnectFuture, (usize, ConnectError)>{
        match timeout(self.timeout_dur, self.socket.read())
                .await
                .map_err(|err| { (self.client_id, ConnectError::from(err))})?
                .map_err(|err| { (self.client_id, ConnectError::from(err))})?
        {

            VariablePacket::ConnectPacket(connect_packet) => {

                // Request plugin to authenticate the user
                let authn_result = self.plugin_requester
                                       .authentication(self.client_id,
                                                       connect_packet.clone()).await;

                let (connack, err) = match authn_result {
                    Ok(true) => {
                        let keep_alive = connect_packet.keep_alive();

                        self.keep_alive = if keep_alive == 0 {
                            None
                        } else {
                            Some(keep_alive as u64)
                        };

                        (ConnackPacket::new(false, ConnectReturnCode::ConnectionAccepted), None)
                    }
                    Ok(false) => {
                        (ConnackPacket::new(false, ConnectReturnCode::NotAuthorized), None)
                    }
                    Err(_err) => {
                        (ConnackPacket::new(false, ConnectReturnCode::NotAuthorized),
                         Some(ConnectError::AuthenticationPluginError))
                    }
                };
                
                let mut buf = vec![];

                if let Err(err) = connack.encode(&mut buf) {
                    log::error!("Can't encode connack packet. {}", err);
                }

                self.socket.write_all(buf.as_ref())
                    .await
                    .map_err(|err| { (self.client_id, ConnectError::from(err))})?;

                if let Some(err) = err {
                    Err((self.client_id, err))
                } else {
                    Ok(self)
                }
            },
            _ => Err((self.client_id, ConnectError::WrongPacket)),
        }
    }
}

pub enum ConnectError {
    SocketError(SocketError),
    WrongPacket,
    Timeout,
    AuthenticationPluginError,
}

impl Display for ConnectError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            ConnectError::SocketError(socket_error) => {
                write!(f, "Got socket error while establishing mqtt level connection. {}", socket_error)
            },
            ConnectError::WrongPacket => {
                f.write_str("Wrong packet came instead of mqtt connect packet.")
            },
            ConnectError::Timeout => {
                f.write_str("Mqtt connection timeout expired. Mqtt connect packet didn't arrive in time.")
            },
            ConnectError::AuthenticationPluginError => {
                f.write_str("Authentication plugin response error.")
            }
        }
    }
}


impl From<SocketError> for ConnectError {
    fn from(socket_error: SocketError) -> Self {
        ConnectError::SocketError(socket_error)
    }
}

impl From<Elapsed> for ConnectError {
    fn from(_elapsed: Elapsed) -> Self {
        ConnectError::Timeout
    }
}