mod keepalive_task;
mod client_message_handler;
mod connect_fut;
mod socket_handler;

pub use connect_fut::{ConnectFuture, ConnectError};
pub use client_message_handler::create_and_serialize_publish_packet;
use crate::broker::{BrokerCommand, ClientEvent};
use crate::socket::Socket;
use futures::future::abortable;
use tokio::sync::mpsc::{channel, Sender, Receiver, UnboundedSender};
use tokio::sync::RwLock;
use tokio::time::sleep;
use std::sync::Arc;
use std::time::{Instant, Duration};
use crate::plugin::plugin_requester::{PluginRequester, DisconnectReason};
use crate::metrics;
use crate::conf;

use self::client_message_handler::ClientMessageHandlerTask;
use self::socket_handler::SocketHandler;
use self::keepalive_task::KeepaliveTask;


pub struct Client {
    client_id        : usize,
    data_rx_outer    : Option<crate::channel::Receiver<Arc<Vec<u8>>>>,
    data_rx_inner    : Option<Receiver<Vec<u8>>>,
    data_tx_inner    : Sender<Vec<u8>>,
    command_channel  : Option<Receiver<BrokerCommand>>,
    event_channel    : UnboundedSender<ClientEvent>,
    socket           : Option<Socket>,
    last_activity    : Arc<RwLock<Instant>>,
    plugin           : PluginRequester,
    keep_alive       : Option<u64>,
}

impl Client {
    pub fn new(client_id       : usize,
               data_rx         : crate::channel::Receiver<Arc<Vec<u8>>>,
               command_channel : Receiver<BrokerCommand>,
               event_channel   : UnboundedSender<ClientEvent>,
               socket          : Socket,
               plugin          : PluginRequester,
               keep_alive      : Option<u64>) -> Self {
        let (data_tx_inner, data_rx_inner) = channel(32);

        crate::increase_counter!(metrics::CLIENT_OBJ_COUNT);
        
        Self {
            client_id,
            data_tx_inner,
            data_rx_inner: Some(data_rx_inner),
            data_rx_outer: Some(data_rx),
            command_channel: Some(command_channel),
            event_channel,
            socket: Some(socket),
            last_activity: Arc::new(RwLock::new(Instant::now())),
            plugin,
            keep_alive,
        }
    }

    pub async fn broker_command( mut command_channel: Receiver<BrokerCommand> ) {
        loop {
            if let Some(broker_command) = command_channel.recv().await {
                match broker_command {
                    BrokerCommand::ForceDisconnect => {
                        // log::info!("Disconnect message from broker has been received!");
                        let delay_sec = conf::get_force_disconnect_delay_sec();
                        sleep(Duration::from_secs(delay_sec)).await;

                        break
                    }
                    BrokerCommand::Publish(_topic, _packet) => {
                        log::debug!("Publish message from broker has been received!");
                    }
                }
            } else {
                log::error!("Broker command channel is broken!");
                break 
            }
        }
    }

    pub async fn run(mut self) {
        let (packet_tx, packet_rx) = channel(1024);
        let socket_handler = SocketHandler::new(self.data_rx_outer.take().unwrap(),
                                                self.data_rx_inner.take().unwrap(),
                                                self.socket.take().unwrap(),
                                                packet_tx);
        let (socket_task, socket_task_abort_handle) = abortable(socket_handler.run());

        let socket_task = tokio::spawn(socket_task);

        let message_handling_task = ClientMessageHandlerTask::new(self.client_id,
                                self.last_activity.clone(),
                                self.event_channel.clone(),
                                packet_rx,
                                self.data_tx_inner.clone(),
                                self.plugin.clone());

        let broker_command_listener = Client::broker_command(self.command_channel.take().unwrap());
        
        let keepalive_task = KeepaliveTask::new(self.keep_alive,
                                                self.last_activity.clone());

        let disconnect_reason = tokio::select!(
            _ = socket_task => {
                DisconnectReason::ClientDisconnected
            },
            _ = message_handling_task.run() => {
                DisconnectReason::ClientDisconnected
            }
            _ = broker_command_listener => {
                DisconnectReason::PluginRequested
            }
            _ = keepalive_task.run() => {
                DisconnectReason::KeepaliveExpired
            }
        );
        
        socket_task_abort_handle.abort();

        if self.plugin.disconnection(self.client_id, disconnect_reason).await.is_err() {
            log::error!("Couldn't send disconnection event to plugin.");
        }
    

        if let Err(err) = self.event_channel.send(ClientEvent::Disconnected(self.client_id)) {
            log::error!("Couldn't send disconnection event to broker. {}", err);
        }

    }
}

impl Drop for Client {
    fn drop(&mut self) {
        crate::decrease_counter!(metrics::CLIENT_OBJ_COUNT);
    }
}