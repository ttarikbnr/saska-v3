use tokio::sync::mpsc::{Sender, Receiver, UnboundedSender};
use tokio::sync::RwLock;
use std::sync::Arc;
use std::time::Instant;
use mqtt::packet::{Packet, VariablePacket, SubscribePacket,
    SubackPacket, PingrespPacket, PublishPacket, UnsubscribePacket, UnsubackPacket};
use mqtt::encodable::Encodable;
use mqtt::packet::suback::SubscribeReturnCode;
use crate::plugin::plugin_requester::PluginRequester;
use crate::plugin::authz_result::AuthzResult;
use crate::broker::ClientEvent;
use VariablePacket::*;

pub struct ClientMessageHandlerTask {
    client_id               : usize,
    last_activity           : Arc<RwLock<Instant>>,
    event_channel           : UnboundedSender<ClientEvent>,
    packet_rx               : Receiver<VariablePacket>,
    data_tx                 : Sender<Vec<u8>>,
    plugin                  : PluginRequester,
}

impl ClientMessageHandlerTask {
    pub fn new(client_id            : usize,
               last_activity        : Arc<RwLock<Instant>>,
               event_channel        : UnboundedSender<ClientEvent>,
               packet_rx            : Receiver<VariablePacket>,
               data_tx              : Sender<Vec<u8>>,
               plugin               : PluginRequester) -> Self {

        Self {
            client_id,
            last_activity,
            event_channel,
            packet_rx,
            data_tx,
            plugin,
        }
    }


    pub async fn run(mut self) {
        loop {
            match self.packet_rx.recv().await {
                Some(packet) => {

                    self.update_last_activity().await;

                    match packet {
                        ConnectPacket(connect_packet) => {
                            log::error!("Connect after connect. {:?}", connect_packet);
                        },
                        SubscribePacket(subscribe_packet) => {
                            let plugin_reply = self.plugin.sub_authorization(self.client_id, subscribe_packet.clone()).await;
                          
                            let authorization_result = match plugin_reply {
                                Ok(authorization_result) => {
                                    authorization_result
                                }
                                Err(_err) => { // TODO return error?
                                    AuthzResult::all_false(subscribe_packet.payload_ref().subscribes().len())
                                }
                            };

                            self.handle_subscribe_packet(subscribe_packet, authorization_result).await;
                        },
                        UnsubscribePacket(unsubscribe_packet) => {
                            self.handle_unsubscribe_packet(unsubscribe_packet).await;
                        },

                        PingreqPacket(_) => {
                            self.handle_pingreq_packet().await;
                        },
                        DisconnectPacket(_disconnect_packet) => {
                            self.handle_disconnect_packet().await;
                            break;
                        },
                        PublishPacket(publish_packet)  => {
                            self.handle_publish_packet(publish_packet).await;
                        }
                        _ => {}
                    }
                },
                None => {
                    log::debug!("None came from packet receiver!");
                    break;
                }
            }
        }
    }

    async fn update_last_activity(&self) {
        *self.last_activity.write().await = Instant::now();
    }

    #[inline(always)]
    async fn handle_subscribe_packet(&mut self,
                                     subscribe_packet   : SubscribePacket,
                                     authz_res      : AuthzResult)  {
        let mut buf = vec![];
        let pkid = subscribe_packet.packet_identifier();
        let mut subacks = vec![];
        let mut  authorized_topics: Vec<String> = vec![];
        for ((topic_filter, _qos), authz_res) in subscribe_packet.payload()
                                                            .subscribes()
                                                            .iter()
                                                            .zip(authz_res.iter()) {
            if authz_res {
                let topic : String = (**topic_filter).to_string();
                authorized_topics.push(topic);
                subacks.push(SubscribeReturnCode::MaximumQoSLevel0);
            } else {
                subacks.push(SubscribeReturnCode::Failure);
            }
        }

        let client_event = ClientEvent::Subscribed(self.client_id, authorized_topics);
        if let Err(_err) = self.event_channel.send(client_event) {
            log::error!("Couldn't send subscribed event to broker task.");
        }

        let suback = SubackPacket::new(pkid, subacks);

        if let Err(err) = suback.encode(&mut buf) {
            log::error!("Can't encode suback packet. {}", err);
        }

        let _ = self.data_tx.send(buf).await;
    }

    async fn handle_unsubscribe_packet(&mut self, unsubscribe_packet: UnsubscribePacket) {
        let topics = unsubscribe_packet.payload_ref()
                                       .subscribes()
                                       .iter()
                                       .map(|topic_filter| {
                                            (**topic_filter).to_string()
                                       })
                                       .collect::<Vec<String>>();

        let unsuback = UnsubackPacket::new(unsubscribe_packet.packet_identifier());

        self.plugin.unsubscribe(self.client_id, unsubscribe_packet).await;

        let client_event = ClientEvent::Unsubscribed(self.client_id, topics);
        if let Err(_err) = self.event_channel.send(client_event) {
            log::error!("Couldn't send subscribed event to broker task.");
        }

        let mut buf = vec![];

        if let Err(err) = unsuback.encode(&mut buf) {
            log::error!("Can't encode unsuback packet. {}", err);
        }

        let _ = self.data_tx.send(buf).await;
    }

    #[inline(always)]
    async fn handle_pingreq_packet(&mut self) {
        let mut buf = vec![];
        let pingresp = PingrespPacket::new();

        if let Err(err) = pingresp.encode(&mut buf) {
            log::error!("Can't encode ping response packet. {}", err);
        }

        let _ = self.data_tx.send(buf).await;
        
    }

    #[inline(always)]
    async fn handle_disconnect_packet(&mut self) {}

    async fn handle_publish_packet(&mut self, publish_packet : PublishPacket) {
        let topic = publish_packet.topic_name().to_string();
        let data : Vec<u8> = publish_packet.payload();
        let client_information = ClientEvent::Published(topic, data);
        let _ = self.event_channel.send(client_information);
    }
}


fn create_publish_packet(topic: String, payload: Vec<u8>) -> PublishPacket{
    let topic_name = mqtt::topic_name::TopicName::new(topic).expect("Couldn't create topic name.");
    let qos =  mqtt::packet::publish::QoSWithPacketIdentifier::Level0;
    PublishPacket::new(topic_name, qos, payload)
}

pub fn create_and_serialize_publish_packet(topic: String, payload: Vec<u8>) -> Vec<u8> {
    let publish_packet = create_publish_packet(topic, payload);
    let mut buf = vec![];

    if let Err(err) = publish_packet.encode(&mut buf) {
        log::error!("Can't encode publis packet. {}", err);
    }

    buf
}
